# GSM Re-Transmitter

SMS Receiver/MMS Transmitter based on the ARM STM32F373RCT6 and Mikroe GSM 2 (Quectel M95).

1) Initialize STM32F373
2) Initialize and set Mikroe GSM 2
3) Idle empty AT command mode 
4) SMS Receive and publish via UART
5) MMS data upload via UART and send
6) Reset system dialing the module
